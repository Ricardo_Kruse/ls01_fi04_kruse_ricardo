package Fassi;

public class Temperaturtabelle {
	public static void main(String[] args) {
		System.out.printf("%-12s", "Fahrenheit");				//kopfzeile
		System.out.print("|");
		System.out.printf("%10s", "Celsius\n");
		
		System.out.printf("%.23s", "---------------------------------------------------------------"); //horizontale Linie
		
		double a= -28.8889;
		double b= -23.3333;
		double c= -17.7778;
		double d= -6.6667;
		double e= -1.1111;
		
		System.out.printf("\n%-12s", "-20");					//zeile 1
		System.out.print("|");
		System.out.printf("%10.2f\n", a);
		
		System.out.printf("%-12s", "-10");						//zeile 2
		System.out.print("|");
		System.out.printf("%10.2f\n", b);
		
		System.out.printf("%-12s", "+0");						//zeile 3
		System.out.print("|");
		System.out.printf("%10.2f\n", c);
	
		System.out.printf("%-12s", "+20");						//zeile 4
		System.out.print("|");
		System.out.printf("%10.2f\n", d);
		
		System.out.printf("%-12s", "+30");						//zeile 5
		System.out.print("|");
		System.out.printf("%10.2f\n", e);
		
	}
}
