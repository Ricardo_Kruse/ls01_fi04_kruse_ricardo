import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
      double[] x;
      int anzahl;
      double m;
      
      Scanner scan = new Scanner(System.in);
      
      System.out.print("Wie viele Zahlen wollen Sie eingeben: ");
      anzahl = scan.nextInt();
      x = new double[anzahl];
  
      for (int i = 0; i < anzahl; i++) {
    	  System.out.print("Bitte geben Sie eine Zahl ein: ");
    	  x[i] = scan.nextDouble();
      }
      
      
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      m = berechneMittelwert(x);
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert  ist %.2f\n", m);
   }

private static double berechneMittelwert(double[] x) {
	// TODO Auto-generated method stub
	double akku = 0;
	for(int i = 0; i < x.length; i++) {
		akku+=x[i];
	}
	return akku/x.length;
}
}