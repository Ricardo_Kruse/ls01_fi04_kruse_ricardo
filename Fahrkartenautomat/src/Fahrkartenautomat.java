﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)        
    {
       Scanner tastatur = new Scanner(System.in);
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       boolean abfrage = true;
       String eingabe;
       
       do {
       	do {
       		 zuZahlenderBetrag = fahrkartenbestellungErfassen(); 
       	
       	} while (zuZahlenderBetrag == 0.0);		//Aufruf der Methode fahrkartenbestellungErfassen, solange die Anzahl der Fahrkarten größer als 10 ist
    	  
      
       
       eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

       fahrkartenAusgeben();

       rueckgeldAusgeben(zuZahlenderBetrag, eingezahlterGesamtbetrag); 
       
       System.out.println("\n Möchten Sie eine weitere Bestellung tätigen?");
       do {
    	   eingabe = tastatur.nextLine().toLowerCase();
    	   if (eingabe.equals("ja")) {
    		   abfrage = true;
    	   }
    	   else if (eingabe.equals("nein")) {
    		   abfrage = false;
    		   System.out.println("Vielen Dank für Ihre Bestellung. Wir wünschen Ihnen einen guten Tag.");
    	   }
    	   else {
    		   System.out.println("Bitte geben Sie 'ja' oder 'nein' ein!");
    		   System.out.println(eingabe);
    		   eingabe = "";
    	   }
       	}while (eingabe.isEmpty());
       
       }while (abfrage == true);
       
    }
    
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	double preise[] = {2.9, 3.3, 3.6, 1.9, 8.6, 9.0, 9.6, 23.5, 24.3, 24.9};
    	String fahrkarte[] = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC",
    			"Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC",
    			"Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};    	
    	double gesamtBetrag = 0;
    	int anzahlFahrkarten;
    	int auswahl;
    	boolean bezahlen = false;
    		System.out.println("Fahrkartenbestellung");
    		for (int j = 0; j < 20; j++)
            {
               System.out.print("=");
               try {
     			Thread.sleep(50);
     		} catch (InterruptedException e) {
     			
     			e.printStackTrace();
     		}               
            }
            System.out.println("\n\n");
            for(int i = 0; i <preise.length; i++) {
            	System.out.printf((i+1) + " %5s " + " %5s " + "€\n", fahrkarte[i], preise[i]);
            }
            System.out.printf((preise.length+1) + " %5s \n", "Bezahlen");
            
    		do {
    			
    			System.out.println("Ihre Auswahl: ");
    			auswahl = tastatur.nextInt();
    			if (auswahl < 1 || auswahl > (preise.length+1)) {
    				System.out.println("Bitte wählen Sie eine Fahrkarte (1-10) aus!");
    				bezahlen = false;
    			}
    			else if(auswahl == (preise.length+1)) {
    				bezahlen = true;  
    				gesamtBetrag = Math.round(gesamtBetrag * 100);
        			gesamtBetrag = gesamtBetrag / 100;
    				return gesamtBetrag;
    			}
    			else {    				
    			System.out.println("Sie haben " + fahrkarte[auswahl-1] + " für den Preis von " + preise[auswahl-1] + "€ gewählt.");
    			
    			System.out.println("Wie viele Karten möchten Sie kaufen? (min. 1, max. 10): ");
    			do {
    				anzahlFahrkarten = tastatur.nextInt();
    				if (anzahlFahrkarten < 1 || anzahlFahrkarten > 10) {
    					System.out.println("Bitte geben Sie eine Zahl zwischen 1 und 10 ein.");
    				}
    			} while(anzahlFahrkarten < 1 || anzahlFahrkarten > 10);
    		
    			gesamtBetrag += (preise[auswahl-1] * anzahlFahrkarten);   			
    			bezahlen = false;
    			}
    			
    	}while (bezahlen == false);
    	return gesamtBetrag;	
    }
        
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	double eingezahlterGesamtbetrag = 0.0;
    	boolean münze;
    	Scanner tastatur = new Scanner(System.in);
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   double ausstehenderBetrag = zuZahlenderBetrag - eingezahlterGesamtbetrag;
     	   System.out.printf("Noch zu zahlen: %.2f €\n" , ausstehenderBetrag);
     	   do {
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   double eingeworfeneMünze = tastatur.nextDouble();
     	   if (eingeworfeneMünze== 2 || eingeworfeneMünze == 1 || eingeworfeneMünze == 0.5 || 
     			   eingeworfeneMünze == 0.2 || eingeworfeneMünze == 0.1 || eingeworfeneMünze == 0.05 ) {
     		   eingezahlterGesamtbetrag += eingeworfeneMünze;
     		   münze = true;
     	   }
     	   else {
     		   System.out.println("Das Bezahlen erfolgt nur mit Münzen! (5ct, 10ct, 20ct, 50ct, 1€, 2€)");
     		   münze = false;
     	   }
     	   }while(münze == false);
        }
        return eingezahlterGesamtbetrag;  
    }
    
    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
        
    public static double rueckgeldAusgeben(double zuZahlenderBetrag, double eingezahlterGesamtbetrag) {
    	double rückgabebetrag = Math.round((eingezahlterGesamtbetrag - zuZahlenderBetrag) * 100);
    	rückgabebetrag = rückgabebetrag / 100;
    	double muenzWert;
    		if(rückgabebetrag > 0.00)
    		{
    			System.out.printf("Der Rückgabebetrag in Höhe von %.2f €\n" , rückgabebetrag);
    			System.out.println("wird in folgenden Münzen ausgezahlt:");

    			while(rückgabebetrag >= 2.00) // 2 EURO-Münzen
				{
				muenzWert= 2.0;
				muenzeDarstellen(muenzWert);
				rückgabebetrag -= 2.0;
				rückgabebetrag = Math.round(rückgabebetrag * 100);
		    	rückgabebetrag = rückgabebetrag / 100;
				}
			while(rückgabebetrag >= 1.00) // 1 EURO-Münzen
				{
				muenzWert = 1.0;
				muenzeDarstellen(muenzWert);
				rückgabebetrag -= 1.0;
				rückgabebetrag = Math.round(rückgabebetrag * 100);
		    	rückgabebetrag = rückgabebetrag / 100;
				}
			while(rückgabebetrag >= 0.50) // 50 CENT-Münzen
			{
				muenzWert = 0.5;
				muenzeDarstellen(muenzWert);
				rückgabebetrag -= 0.5;
				rückgabebetrag = Math.round(rückgabebetrag * 100);
		    	rückgabebetrag = rückgabebetrag / 100;
			}
			while(rückgabebetrag >= 0.20) // 20 CENT-Münzen
			{
				muenzWert = 0.2;
				muenzeDarstellen(muenzWert);
				rückgabebetrag -= 0.2;
				rückgabebetrag = Math.round(rückgabebetrag * 100);
		    	rückgabebetrag = rückgabebetrag / 100;
			}
			while(rückgabebetrag >= 0.10) // 10 CENT-Münzen
			{
				muenzWert= 0.1;
				muenzeDarstellen(muenzWert);
				rückgabebetrag -= 0.1;
				rückgabebetrag = Math.round(rückgabebetrag * 100);
		    	rückgabebetrag = rückgabebetrag / 100;
			}
			while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				muenzWert = 0.05;
				muenzeDarstellen(muenzWert);
				rückgabebetrag -= 0.05;
				rückgabebetrag = Math.round(rückgabebetrag * 100);
		    	rückgabebetrag = rückgabebetrag / 100;
			}
		}
    		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                 "vor Fahrtantritt entwerten zu lassen!\n"+
                 "Wir wünschen Ihnen eine gute Fahrt.");
    		return rückgabebetrag;
     }
    
    public static void muenzeDarstellen(double muenzWert) {
    	System.out.printf("\n%8s" + "%3s" + "%3s\n", "*", "*", "*");
    	System.out.printf("%6s" + "%10s\n", "*", "*");
    	if(muenzWert >= 1.0) {
    		System.out.printf("%5s" + "%6s" + "%6s\n", "*", (int)muenzWert, "*");
    		System.out.printf("%5s" + "%8s" + "%4s\n", "*", "Euro", "*");
    	}
    	else {
    		System.out.printf("%5s" + "%6s" + "%6s\n", "*", (int)(muenzWert*100), "*");
    		System.out.printf("%5s" + "%8s" + "%4s\n", "*", "Cent", "*");
    		}
    	System.out.printf("%6s" + "%10s\n", "*", "*");
    	System.out.printf("%8s" + "%3s" + "%3s\n", "*", "*", "*");
    }
} 